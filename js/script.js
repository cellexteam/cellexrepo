/*jslint browser: true*/
/*global $, jQuery, alert*/
$(function(){
    'use strict';
	$('.nav_trigger').click(function() {
        $('.menu').slideToggle();
    });
    $('.drop > a:first').click(function(e) {
        e.preventDefault();
        $('.sub-menu').slideToggle();
    });
    $('.banner').bxSlider({
        auto: true,
        nextSelector: '#banner_next',
        prevSelector: '#banner_prev',
        nextText: '<i class="fa fa-angle-right"></i>',
        prevText: '<i class="fa fa-angle-left"></i>',
        onSliderLoad: function () {
            $('.banner>li .cap_block').eq(1).addClass('active-slide');
            $(".cap_block.active-slide").addClass("wow animated fadeInUp");
        },
        onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
            //console.log(currentSlideHtmlObject);
            $('.active-slide').removeClass('active-slide');
            $('.banner>li .cap_block').eq(currentSlideHtmlObject + 1).addClass('active-slide');
            $(".cap_block.active-slide").addClass("wow animated fadeInUp");

        },
        onSlideBefore: function () {
            $(".cap_block.active-slide").removeClass("wow animated fadeInUp");
            $(".one.cap_block.active-slide").removeAttr('style');
        }
    });
    $('.testimonial_slider').bxSlider({
        auto: true,
        speed: 1000,
        pager:false,
        easing: 'ease-out'
    });

    // Dynamic pop up for sofa
    $(document).on('click', '.visit a', function (e) {
        e.preventDefault();
        var a = $(this),
            dynamicData = a.attr('data-bind'),
            imgSrc = a.find('img').attr('src'),
            title = a.siblings('h4').text(),
            popTarget = $(a.attr('data-target')),
            dynamicArray = dynamicData.split(','),
            dynamicCount = dynamicArray.length,
            i;
        popTarget.find('img').attr('src', imgSrc);
        popTarget.find('h4').text(title);
        popTarget.find('.details p').empty();
        if ((dynamicCount % 2) == 0) {
            for (i = 0; i < (dynamicCount / 2); i++) {
                popTarget.find('.details p:first').append(dynamicArray[i] + '<br />');
            }
            for (i = (dynamicArray / 2); i < dynamicCount; i++) {
                popTarget.find('.details p:last').append(dynamicArray[i] + '<br />');
            }
        } else {
            var divider = Math.ceil(dynamicCount / 2);
            for (i = 0; i < divider; i++) {
                popTarget.find('.details p:first').append(dynamicArray[i] + '<br />');
            }
            for (i = divider; i< dynamicCount; i++) {
                popTarget.find('.details p:last').append(dynamicArray[i] + '<br />');
            }
        }
        popTarget.modal('show');
    });
});
   WebFontConfig = {
    google: { families: [ 'Lato::latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })(); 

/*$(function () {
    var slider = $('.banner').bxSlider({
        pager: false,
        auto: true,
        prevSelector: '#mob_prev',
        nextSelector: '#mob_next',
        prevText: '<i class="ion ion-android-arrow-dropleft"></i>',
        nextText: '<i class="ion ion-android-arrow-dropright"></i>',
        onSlideAfter: function() {
            slider.stopAuto();
            slider.startAuto();
        }
    });*/
